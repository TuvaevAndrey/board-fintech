package com.fintech.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fintech.model.Board;
import com.fintech.model.Note;
import com.fintech.model.Schedule;
import com.fintech.model.Task;
import com.fintech.repository.TagRepository;
import com.fintech.service.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.*;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class TagControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private TagController tagController;
    @Autowired
    private NoteService noteService;
    @Autowired
    private BoardService boardService;
    @Autowired
    private TaskService taskService;
    @Autowired
    private ScheduleService scheduleService;
    @Autowired
    private TagService tagService;
    @Autowired
    private TagRepository tagRepository;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private Note someNote = prepareNote("Note", "Some note");
    private Board someBoard = prepareBoard("Board", "Some board");
    private Task someTask = prepareTask("Some task", "Task for test", "Anny");
    private Schedule someSchedule = prepareSchedule("Schedule", "Some schedule");

    private static final ObjectMapper mapper = new ObjectMapper();

    @AfterEach
    void resetDb() {
        boardService.allBoards().forEach(board -> boardService.delete(board.getId()));
        noteService.allNotes().forEach(note -> noteService.delete(note.getId()));
        scheduleService.allSchedules().forEach(schedule -> scheduleService.delete(schedule.getId()));
    }

    @Test
    void validAddTagToNote() throws Exception {
        noteService.save(someNote);
        String sql = "SELECT * FROM public.notes WHERE title = '" + someNote.getTitle()
                + "' AND description = '" + someNote.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String tag = "New tag";
        mockMvc.perform(post("/tags/notes/{noteId}?tag={tag}", id, tag)
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
        Note entry = jdbcTemplate.queryForObject(sql, (rs, rowNum) -> new Note(rs.getString("title"),
                rs.getString("description")));
        String sqlTag = "SELECT * FROM public.tags_on_notes WHERE note_id = '" + id + "'";
        Set<String> tags = Set.copyOf(jdbcTemplate.query(sqlTag, (rs, rowNum) -> rs.getString("tag")));
        someNote.setTags(Set.of(tag.toLowerCase()));
        entry.setTags(tags);
        assertEquals(someNote, entry);
    }

    @Test
    void invalidAddTagToNoteNotFound() throws Exception {
        String tag = "New tag";
        long id = 0;
        mockMvc.perform(post("/tags/notes/{noteId}?tag={tag}", id, tag)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Note not found : " + id));
    }

    @Test
    void validAddTagToBoard() throws Exception {
        boardService.save(someBoard);
        String sql = "SELECT * FROM public.boards WHERE title = '" + someBoard.getTitle()
                + "' AND description = '" + someBoard.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String tag = "New tag 2";
        mockMvc.perform(post("/tags/boards/{boardId}?tag={tag}", id, tag)
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
        Board entry = jdbcTemplate.queryForObject(sql, (rs, rowNum) -> new Board(rs.getString("title"),
                rs.getString("description")));
        String sqlTag = "SELECT * FROM public.tags_on_boards WHERE board_id = '" + id + "'";
        Set<String> tags = Set.copyOf(jdbcTemplate.query(sqlTag, (rs, rowNum) -> rs.getString("tag")));
        someBoard.setTags(Set.of(tag.toLowerCase()));
        entry.setTags(tags);
        assertEquals(someBoard, entry);
    }

    @Test
    void invalidAddTagToBoardNotFound() throws Exception {
        String tag = "New tag";
        long id = 0;
        mockMvc.perform(post("/tags/boards/{boardId}?tag={tag}", id, tag)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Board not found : " + id));
    }

    @Test
    void validAddTagToSchedule() throws Exception {
        scheduleService.save(someSchedule);
        String sql = "SELECT * FROM public.schedules WHERE title = '" + someSchedule.getTitle()
                + "' AND description = '" + someSchedule.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String tag = "New tag 3";
        mockMvc.perform(post("/tags/schedules/{scheduleId}?tag={tag}", id, tag)
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
        Schedule entry = jdbcTemplate.queryForObject(sql, (rs, rowNum) -> new Schedule(rs.getString("title"),
                rs.getString("description")));
        String sqlTag = "SELECT * FROM public.tags_on_schedules WHERE schedule_id = '" + id + "'";
        Set<String> tags = Set.copyOf(jdbcTemplate.query(sqlTag, (rs, rowNum) -> rs.getString("tag")));
        someSchedule.setTags(Set.of(tag.toLowerCase()));
        entry.setTags(tags);
        assertEquals(someSchedule, entry);
    }

    @Test
    void invalidAddTagToScheduleNotFound() throws Exception {
        String tag = "New tag";
        long id = 0;
        mockMvc.perform(post("/tags/schedules/{scheduleId}?tag={tag}", id, tag)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Schedule not found : " + id));
    }

    @Test
    void validAddTagToTask() throws Exception {
        boardService.save(someBoard);
        String sql = "SELECT * FROM public.boards WHERE title = '" + someBoard.getTitle()
                + "' AND description = '" + someBoard.getDescription() + "'";
        long boardId = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        taskService.addTaskToBoard(boardId, someTask);
        String sql2 = "SELECT * FROM public.tasks WHERE title = '" + someTask.getTitle()
                + "' AND description = '" + someTask.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql2, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String tag = "New tag 4";
        mockMvc.perform(post("/tags/boards/tasks/{taskId}?tag={tag}", id, tag)
                .contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
        Task entry = jdbcTemplate.queryForObject(sql2, (rs, rowNum) -> new Task(rs.getString("title"),
                rs.getString("description"), rs.getLong("board_id")));
        String sqlTag = "SELECT * FROM public.tags_on_tasks WHERE task_id = '" + id + "'";
        Set<String> tags = Set.copyOf(jdbcTemplate.query(sqlTag, (rs, rowNum) -> rs.getString("tag")));
        someTask.setTags(Set.of(tag.toLowerCase()));
        entry.setTags(tags);
        assertEquals(someTask, entry);
    }

    @Test
    void invalidAddTagToTaskNotFound() throws Exception {
        String tag = "New tag";
        long id = 0;
        mockMvc.perform(post("/tags/boards/tasks/{taskId}?tag={tag}", id, tag)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Task not found : " + id));
    }

    @Test
    void validFindEntitiesByTag() throws Exception {
        boardService.save(someBoard);
        noteService.save(someNote);
        scheduleService.save(someSchedule);
        String sql = "SELECT * FROM public.boards WHERE title = '" + someBoard.getTitle()
                + "' AND description = '" + someBoard.getDescription() + "'";
        long boardId = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String sql2 = "SELECT * FROM public.notes WHERE title = '" + someNote.getTitle()
                + "' AND description = '" + someNote.getDescription() + "'";
        long noteId = (Long) jdbcTemplate.query(sql2, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String sql3 = "SELECT * FROM public.schedules WHERE title = '" + someSchedule.getTitle()
                + "' AND description = '" + someSchedule.getDescription() + "'";
        long scheduleId = (Long) jdbcTemplate.query(sql3, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String tag = "Big tag";
        tagService.addTagToBoard(boardId, tag);
        tagService.addTagToNote(noteId, tag);
        tagService.addTagToSchedule(scheduleId, tag);
        mockMvc.perform(get("/tags?tag={tag}", tag)).andExpect(status().isOk()).andExpect(jsonPath("$..title",
                hasItems(someNote.getTitle(), someBoard.getTitle(), someSchedule.getTitle())));
    }

    @Test
    void invalidFindEntitiesByTagNotFound() throws Exception {
        String tag = "Big tag";
        mockMvc.perform(get("/tags?tag={tag}", tag)).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Tag not found : " + tag));
    }

    @Test
    void validFindNotesByTag() throws Exception {
        noteService.save(someNote);
        String sql = "SELECT * FROM public.notes WHERE title = '" + someNote.getTitle()
                + "' AND description = '" + someNote.getDescription() + "'";
        long noteId = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String tag = "Big tag";
        tagService.addTagToNote(noteId, tag);
        mockMvc.perform(get("/tags/notes?tag={tag}", tag)).andExpect(status().isOk())
                .andExpect(jsonPath("$..title", hasItem(someNote.getTitle())));
    }

    @Test
    void invalidFindNoteByTagNotFound() throws Exception {
        String tag = "Big tag";
        mockMvc.perform(get("/tags/notes?tag={tag}", tag)).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("There are no notes with this tag : " + tag));
    }

    @Test
    void validFindBoardsByTag() throws Exception {
        boardService.save(someBoard);
        String sql = "SELECT * FROM public.boards WHERE title = '" + someBoard.getTitle()
                + "' AND description = '" + someBoard.getDescription() + "'";
        long boardId = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String tag = "Big tag";
        tagService.addTagToBoard(boardId, tag);
        mockMvc.perform(get("/tags/boards?tag={tag}", tag)).andExpect(status().isOk())
                .andExpect(jsonPath("$..title", hasItem(someBoard.getTitle())));
    }

    @Test
    void invalidFindBoardByTagNotFound() throws Exception {
        String tag = "Big tag";
        mockMvc.perform(get("/tags/boards?tag={tag}", tag)).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("There are no boards with this tag : " + tag));
    }

    @Test
    void validFindTasksByTag() throws Exception {
        boardService.save(someBoard);
        String sql = "SELECT * FROM public.boards WHERE title = '" + someBoard.getTitle()
                + "' AND description = '" + someBoard.getDescription() + "'";
        long boardId = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        taskService.addTaskToBoard(boardId, someTask);
        String sql2 = "SELECT * FROM public.tasks WHERE title = '" + someTask.getTitle()
                + "' AND description = '" + someTask.getDescription() + "'";
        long taskId = (Long) jdbcTemplate.query(sql2, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String tag = "Big tag";
        tagService.addTagToTask(taskId, tag);
        mockMvc.perform(get("/tags/boards/tasks?tag={tag}", tag)).andExpect(status().isOk())
                .andExpect(jsonPath("$..title", hasItem(someTask.getTitle())));
    }

    @Test
    void invalidFindTaskByTagNotFound() throws Exception {
        String tag = "Big tag";
        mockMvc.perform(get("/tags/boards/tasks?tag={tag}", tag)).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("There are no tasks with this tag : " + tag));
    }

    @Test
    void validFindSchedulesByTag() throws Exception {
        scheduleService.save(someSchedule);
        String sql = "SELECT * FROM public.schedules WHERE title = '" + someSchedule.getTitle()
                + "' AND description = '" + someSchedule.getDescription() + "'";
        long scheduleId = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String tag = "Big tag";
        tagService.addTagToSchedule(scheduleId, tag);
        mockMvc.perform(get("/tags/schedules?tag={tag}", tag)).andExpect(status().isOk())
                .andExpect(jsonPath("$..title", hasItem(someSchedule.getTitle())));
    }

    @Test
    void invalidFindScheduleByTagNotFound() throws Exception {
        String tag = "Big tag";
        mockMvc.perform(get("/tags/schedules?tag={tag}", tag)).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("There are no schedules with this tag : " + tag));
    }

    @Test
    void validGetTagFromNote() throws Exception {
        noteService.save(someNote);
        String sql = "SELECT * FROM public.notes WHERE title = '" + someNote.getTitle()
                + "' AND description = '" + someNote.getDescription() + "'";
        long noteId = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String tag1 = "Big tag";
        String tag2 = "Little tag";
        tagService.addTagToNote(noteId, tag1);
        tagService.addTagToNote(noteId, tag2);
        mockMvc.perform(get("/tags/notes/{noteId}", noteId)).andExpect(status().isOk())
                .andExpect(jsonPath("$", hasItems(tag1.toLowerCase(), tag2.toLowerCase())));
    }

    @Test
    void invalidGetTagFromNoteNotFound() throws Exception {
        long noteId = 0;
        mockMvc.perform(get("/tags/notes/{noteId}", noteId)).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Note not found : " + noteId));
    }

    @Test
    void validGetTagFromBoard() throws Exception {
        boardService.save(someBoard);
        String sql = "SELECT * FROM public.boards WHERE title = '" + someBoard.getTitle()
                + "' AND description = '" + someBoard.getDescription() + "'";
        long boardId = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String tag1 = "Big tag";
        String tag2 = "Little tag";
        tagService.addTagToBoard(boardId, tag1);
        tagService.addTagToBoard(boardId, tag2);
        mockMvc.perform(get("/tags/boards/{boardId}", boardId)).andExpect(status().isOk())
                .andExpect(jsonPath("$", hasItems(tag1.toLowerCase(), tag2.toLowerCase())));
    }

    @Test
    void invalidGetTagFromBoardNotFound() throws Exception {
        long boardId = 0;
        mockMvc.perform(get("/tags/boards/{boardId}", boardId)).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Board not found : " + boardId));
    }

    @Test
    void validGetTagFromTask() throws Exception {
        boardService.save(someBoard);
        String sql = "SELECT * FROM public.boards WHERE title = '" + someBoard.getTitle()
                + "' AND description = '" + someBoard.getDescription() + "'";
        long boardId = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        taskService.addTaskToBoard(boardId, someTask);
        String sql2 = "SELECT * FROM public.tasks WHERE title = '" + someTask.getTitle()
                + "' AND description = '" + someTask.getDescription() + "'";
        long taskId = (Long) jdbcTemplate.query(sql2, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String tag1 = "Big tag";
        String tag2 = "Little tag";
        tagService.addTagToTask(taskId, tag1);
        tagService.addTagToTask(taskId, tag2);
        mockMvc.perform(get("/tags/boards/tasks/{taskId}", taskId)).andExpect(status().isOk())
                .andExpect(jsonPath("$", hasItems(tag1.toLowerCase(), tag2.toLowerCase())));
    }

    @Test
    void invalidGetTagFromTaskNotFound() throws Exception {
        long taskId = 0;
        mockMvc.perform(get("/tags/boards/tasks/{taskId}", taskId)).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Task not found : " + taskId));
    }

    @Test
    void validGetTagFromSchedule() throws Exception {
        scheduleService.save(someSchedule);
        String sql = "SELECT * FROM public.schedules WHERE title = '" + someSchedule.getTitle()
                + "' AND description = '" + someSchedule.getDescription() + "'";
        long scheduleId = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String tag1 = "Big tag";
        String tag2 = "Little tag";
        tagService.addTagToSchedule(scheduleId, tag1);
        tagService.addTagToSchedule(scheduleId, tag2);
        mockMvc.perform(get("/tags/schedules/{scheduleId}", scheduleId)).andExpect(status().isOk())
                .andExpect(jsonPath("$", hasItems(tag1.toLowerCase(), tag2.toLowerCase())));
    }

    @Test
    void invalidGetTagFromScheduleNotFound() throws Exception {
        long scheduleId = 0;
        mockMvc.perform(get("/tags/schedules/{scheduleId}", scheduleId)).andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Schedule not found : " + scheduleId));
    }

    @Test
    void validDeleteTagFromNote() throws Exception {
        noteService.save(someNote);
        String sql = "SELECT * FROM public.notes WHERE title = '" + someNote.getTitle()
                + "' AND description = '" + someNote.getDescription() + "'";
        long noteId = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String tag1 = "Big tag";
        String tag2 = "Little tag";
        tagService.addTagToNote(noteId, tag1);
        tagService.addTagToNote(noteId, tag2);
        mockMvc.perform(delete("/tags/notes/{noteId}?tag={tag}", noteId, tag1)).andExpect(status().isOk());
        String sql2 = "SELECT * FROM public.tags_on_notes WHERE note_id = '" + noteId + "'";
        List<String> tags = new ArrayList(jdbcTemplate.query(sql2, (rs, rowNum) -> rs.getString("tag")));
        assertTrue(tags.contains(tag2.toLowerCase()) && !tags.contains(tag1.toLowerCase()));
    }

    @Test
    void validDeleteTagFromBoard() throws Exception {
        boardService.save(someBoard);
        String sql = "SELECT * FROM public.boards WHERE title = '" + someBoard.getTitle()
                + "' AND description = '" + someBoard.getDescription() + "'";
        long boardId = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String tag1 = "Big tag";
        String tag2 = "Little tag";
        tagService.addTagToBoard(boardId, tag1);
        tagService.addTagToBoard(boardId, tag2);
        mockMvc.perform(delete("/tags/boards/{boardId}?tag={tag}", boardId, tag1)).andExpect(status().isOk());
        String sql2 = "SELECT * FROM public.tags_on_boards WHERE board_id = '" + boardId + "'";
        List<String> tags = new ArrayList(jdbcTemplate.query(sql2, (rs, rowNum) -> rs.getString("tag")));
        assertTrue(tags.contains(tag2.toLowerCase()) && !tags.contains(tag1.toLowerCase()));
    }

    @Test
    void validDeleteTagFromTask() throws Exception {
        boardService.save(someBoard);
        String sql = "SELECT * FROM public.boards WHERE title = '" + someBoard.getTitle()
                + "' AND description = '" + someBoard.getDescription() + "'";
        long boardId = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        taskService.addTaskToBoard(boardId, someTask);
        String sql2 = "SELECT * FROM public.tasks WHERE title = '" + someTask.getTitle()
                + "' AND description = '" + someTask.getDescription() + "'";
        long taskId = (Long) jdbcTemplate.query(sql2, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String tag1 = "Big tag";
        String tag2 = "Little tag";
        tagService.addTagToTask(taskId, tag1);
        tagService.addTagToTask(taskId, tag2);
        mockMvc.perform(delete("/tags/boards/tasks/{taskId}?tag={tag}", taskId, tag1)).andExpect(status().isOk());
        String sql3 = "SELECT * FROM public.tags_on_tasks WHERE task_id = '" + taskId + "'";
        List<String> tags = new ArrayList(jdbcTemplate.query(sql3, (rs, rowNum) -> rs.getString("tag")));
        assertTrue(tags.contains(tag2.toLowerCase()) && !tags.contains(tag1.toLowerCase()));
    }

    @Test
    void validDeleteTagFromSchedule() throws Exception {
        scheduleService.save(someSchedule);
        String sql = "SELECT * FROM public.schedules WHERE title = '" + someSchedule.getTitle()
                + "' AND description = '" + someSchedule.getDescription() + "'";
        long scheduleId = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        String tag1 = "Big tag";
        String tag2 = "Little tag";
        tagService.addTagToSchedule(scheduleId, tag1);
        tagService.addTagToSchedule(scheduleId, tag2);
        mockMvc.perform(delete("/tags/schedules/{scheduleId}?tag={tag}", scheduleId, tag1)).andExpect(status().isOk());
        String sql2 = "SELECT * FROM public.tags_on_schedules WHERE schedule_id = '" + scheduleId + "'";
        List<String> tags = new ArrayList(jdbcTemplate.query(sql2, (rs, rowNum) -> rs.getString("tag")));
        assertTrue(tags.contains(tag2.toLowerCase()) && !tags.contains(tag1.toLowerCase()));
    }

    private static Note prepareNote(String title, String description) {
        return new Note(title, description);
    }

    private static Board prepareBoard(String title, String description) {
        return new Board(title, description);
    }

    private static Task prepareTask(String title, String description, String employee) {
        return new Task(title, description, employee);
    }

    private static Schedule prepareSchedule(String title, String description) {
        return new Schedule(title, description);
    }
}
