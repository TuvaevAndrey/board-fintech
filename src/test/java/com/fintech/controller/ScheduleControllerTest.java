package com.fintech.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fintech.model.Schedule;
import com.fintech.service.ScheduleService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasItems;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
class ScheduleControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ScheduleController scheduleController;
    @Autowired
    private ScheduleService scheduleService;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final ObjectMapper mapper = new ObjectMapper();

    Schedule normSchedule = prepareSchedule("Schedule", "Some schedule");
    Schedule normSchedule2 = prepareSchedule("Schedule 2", "Some schedule 2");

    @AfterEach
    public void resetDb() {
        scheduleService.allSchedules().forEach(schedule -> scheduleService.delete(schedule.getId()));
    }

    @Test
    void validAddSchedule() throws Exception {
        String json = mapper.writeValueAsString(normSchedule);
        mockMvc.perform(post("/schedules").content(json).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
        String sql = "SELECT * FROM public.schedules WHERE title = '" + normSchedule.getTitle()
                + "' AND description = '" + normSchedule.getDescription() + "'";
        Schedule entry = jdbcTemplate.queryForObject(sql, (rs, rowNum) -> new Schedule(rs.getString("title"),
                rs.getString("description")));
        assertEquals(normSchedule, entry);
    }

    @Test
    void invalidAddSchedule() throws Exception {
        normSchedule.setDescription(null);
        String json = mapper.writeValueAsString(normSchedule);
        mockMvc.perform(post("/schedules").content(json).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    void validGetSchedule() throws Exception {
        scheduleService.save(normSchedule);
        String sql = "SELECT * FROM public.schedules WHERE title = '" + normSchedule.getTitle()
                + "' AND description = '" + normSchedule.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        mockMvc.perform(get("/schedules/{scheduleId}", id).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andExpect(jsonPath("$.title").value(normSchedule.getTitle()))
                .andExpect(jsonPath("$.description").value(normSchedule.getDescription()));
    }


    @Test
    void invalidGetScheduleNotFound() throws Exception {
        long id = 0;
        mockMvc.perform(get("/schedules/{scheduleId}", id).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound()).andExpect(jsonPath("$.message")
                        .value("Schedule not found : " + id));
    }

    @Test
    void validDeleteSchedule() throws Exception {
        scheduleService.save(normSchedule);
        String sql = "SELECT * FROM public.schedules WHERE title = '" + normSchedule.getTitle()
                + "' AND description = '" + normSchedule.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        mockMvc.perform(delete("/schedules/{scheduleId}", id).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
        var newId = jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray();
        assertTrue(List.of(newId).isEmpty());
    }

    @Test
    void validUpdateSchedule() throws Exception {
        scheduleService.save(normSchedule);
        String sql = "SELECT * FROM public.schedules WHERE title = '" + normSchedule.getTitle()
                + "' AND description = '" + normSchedule.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        normSchedule.setDescription("New description");
        String json = mapper.writeValueAsString(normSchedule);
        mockMvc.perform(put("/schedules/{scheduleId}", id).content(json).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
        String sql3 = "SELECT * FROM public.schedules WHERE id = '" + id + "'";
        Schedule entry = jdbcTemplate.queryForObject(sql3, (rs, rowNum) -> new Schedule(rs.getString("title"),
                rs.getString("description")));
        assertEquals(normSchedule, entry);
    }

    @Test
    void invalidUpdateScheduleBadRequest() throws Exception {
        scheduleService.save(normSchedule);
        String sql = "SELECT * FROM public.schedules WHERE title = '" + normSchedule.getTitle()
                + "' AND description = '" + normSchedule.getDescription() + "'";
        long id = (Long) jdbcTemplate.query(sql, (rs, rowNum) -> rs.getLong("id")).toArray()[0];
        normSchedule.setTitle(null);
        String json = mapper.writeValueAsString(normSchedule);
        mockMvc.perform(put("/schedules/{scheduleId}", id).content(json).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isBadRequest());
    }

    @Test
    void invalidUpdateScheduleNotFound() throws Exception {
        long id = 0;
        normSchedule.setTitle("New title");
        String json = mapper.writeValueAsString(normSchedule);
        mockMvc.perform(put("/schedules/{scheduleId}", id).content(json).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNotFound()).andExpect(jsonPath("$.message")
                        .value("Schedule not found : " + id));
    }

    @Test
    void validGetAllSchedules() throws Exception {
        scheduleService.save(normSchedule);
        scheduleService.save(normSchedule2);
        mockMvc.perform(get("/schedules").contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andExpect(jsonPath("$..title",
                        hasItems(normSchedule.getTitle(), normSchedule2.getTitle())));
    }

    private static Schedule prepareSchedule(String title, String description) {
        return new Schedule(title, description);
    }

}