package com.fintech.service;

import com.fintech.model.Board;
import com.fintech.model.Task;
import com.fintech.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskService {

    private final TaskRepository taskRepository;
    private final BoardService boardService;

    public void addTaskToBoard(long boardId, Task task) {
        Board board = boardService.findBoard(boardId);
        task.setBoardId(boardId);
        if (!board.getStatus().contains(task.getStatus())) {
            task.setStatus(null);
        }
        taskRepository.save(task);
    }

    @CacheEvict(value = "tasks", key = "#taskId")
    public void setStatus(long boardId, long taskId, String status) {
        Board board = boardService.findBoard(boardId);
        if (!board.getStatus().contains(status)) {
            status = null;
        }
        taskRepository.changeStatus(taskId, status);
    }

    public List<Task> allTasksFromBoard(long boardId) {
        return taskRepository.findByBoardId(boardId);
    }

    @Cacheable(value = "tasks", key = "#id", unless = "#result == null")
    public Task findTaskById(long id) {
        return taskRepository.findByTaskId(id);
    }

    @CacheEvict(value = "tasks", key = "#taskId")
    public void updateTask(Task task, long taskId) {
        task.setId(taskId);
        taskRepository.update(task);
    }

    @CacheEvict(value = "tasks", key = "#taskId")
    public void replaceTaskToAnotherBoard(long newBoardId, Task task, long taskId) {
        task.setId(taskId);
        Board newBoard = boardService.findBoard(newBoardId);
        task.setBoardId(newBoardId);
        if (!newBoard.getStatus().contains(task.getStatus())) {
            task.setStatus(null);
        }
        taskRepository.replace(taskId, newBoardId, task.getStatus());
    }

    @CacheEvict(value = "tasks", key = "#id")
    public void deleteTask(long id) {
        taskRepository.delete(id);
    }

}
