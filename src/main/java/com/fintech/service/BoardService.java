package com.fintech.service;

import com.fintech.model.Board;
import com.fintech.repository.BoardRepository;
import com.fintech.repository.StatusRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BoardService {

    private final BoardRepository repository;
    private final StatusRepository statusRepository;

    public void save(Board board) {
        repository.save(board);
    }

    @CacheEvict(value = "boards", key = "#boardId")
    public void addStatus(long boardId, String status) {
        statusRepository.save(boardId, status);
    }

    @CacheEvict(value = "boards", key = "#boardId")
    public void deleteStatus(long boardId, String status) {
        statusRepository.delete(boardId, status);
    }

    @CacheEvict(value = "boards", key = "#boardId")
    public void update(Board board, long boardId) {
        board.setId(boardId);
        repository.update(board);
    }

    @CacheEvict(value = "boards", key = "#id")
    public void delete(long id) {
        repository.delete(id);
    }

    public List<Board> allBoards() {
        return repository.findAll();
    }

    @Cacheable(value = "boards", key = "#id", unless = "#result == null")
    public Board findBoard(long id) {
        return repository.findById(id);
    }

}
