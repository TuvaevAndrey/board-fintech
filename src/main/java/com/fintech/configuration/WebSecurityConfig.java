package com.fintech.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.InMemoryUserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CredentialsKeeper credentialsKeeper;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        try {
            InMemoryUserDetailsManagerConfigurer<AuthenticationManagerBuilder> builder = auth.inMemoryAuthentication();
            credentialsKeeper.getCredentials().forEach(
                user -> builder.withUser(user.getUsername()).password("{noop}" + user.getPassword())
                    .roles(user.getRole().toString()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void configure(HttpSecurity http) {
        try {
            http.csrf().disable().authorizeRequests()
                .antMatchers("/swagger-ui/**", "/v3/api-docs/**", "/swagger/**", "/swagger*").permitAll()
                .antMatchers(HttpMethod.GET, "/**").hasAnyAuthority("ROLE_USER", "ROLE_ADMIN")
                .antMatchers(HttpMethod.POST, "/**").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.PATCH, "/**").hasAuthority("ROLE_ADMIN")
                .antMatchers(HttpMethod.DELETE, "/**").hasAuthority("ROLE_ADMIN")
                .anyRequest().authenticated()
                .and().httpBasic();
            http.headers().frameOptions().disable();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}