package com.fintech.repository;

import com.fintech.model.Board;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BoardRepository {

    void save(Board board);

    void update(Board board);

    void delete(long id);

    List<Board> findAll();

    Board findById(long id);

    List<Board> findBoardsById(List<Long> id);

}
