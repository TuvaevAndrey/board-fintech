package com.fintech.repository;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface StatusRepository {

    void save(long id, String status);

    void delete(long id, String status);

    List<String> findById(long id);

}
