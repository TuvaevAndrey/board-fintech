package com.fintech.repository;

import com.fintech.model.SchedulePlan;
import org.apache.ibatis.annotations.Mapper;

import java.time.LocalDate;
import java.util.List;

@Mapper
public interface SchedulePlanRepository {

    void save(long id, LocalDate date, String plan);

    void delete(long id);

    List<SchedulePlan> findAll();

    SchedulePlan findById(long id);

    List<SchedulePlan> findByScheduleId(long id);

    List<SchedulePlan> findByScheduleIdAndTwoDate(long id, LocalDate fromDate, LocalDate toDate);

}
