package com.fintech.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Set;

@Data
@RequiredArgsConstructor
public class Schedule {

    private long id;
    @NotNull
    private String title;
    @NotNull
    private String description;
    private LocalDateTime current;
    private Set<String> tags;
    private LocalDateTime timeOfCreation;
    private LocalDateTime timeOfLastChange;

    public Schedule(String title, String description) {
        this.title = title;
        this.description = description;
    }
}
