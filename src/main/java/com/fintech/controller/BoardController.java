package com.fintech.controller;

import com.fintech.model.Board;
import com.fintech.model.Task;
import com.fintech.service.BoardService;
import com.fintech.service.TaskService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.fintech.exception.AppExceptions.BOARD_NOT_FOUND;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@OpenAPIDefinition(
        info = @Info(
                title = "board-controller",
                version = "1.0",
                description = "Контроллер для взаимодействия с досками"
        )
)
@RestController
@RequestMapping("/boards")
@RequiredArgsConstructor
@Slf4j
public class BoardController {

    private final BoardService boardService;
    private final TaskService taskService;

    @Operation(summary = "Добавить новую доску",
            description = "Добавляет доску (Request Body) с ненулевыми значениями: названием title и описанием description; " +
                    "присваивает id, время создания timeOfCreation и время последнего изменения timeOfLastChange " +
                    "в БД; теги Set<String> tags, статусы Set<String> status и задачи Set<Task> tasks в этом " +
                    "методе игнорируются, и могут быть добавлены на доску другими методами")
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public void addBoard(@RequestBody @Valid Board board) {
        log.info("BoardController.addBoard.in  Board - {}", board);
        boardService.save(board);
        log.info("BoardController.addBoard.out");
    }

    @Operation(summary = "Получить список всех досок",
            description = "Возвращает список всех досок: с id, названием title, описанием description, " +
                    "временем последнего изменения timeOfLastChange и создания timeOfCreation, " +
                    "статусами Set<String> status и тегами Set<String> tags")
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public List<Board> getAllBoards() {
        log.info("BoardController.getAllBoards.in");
        List<Board> boards = boardService.allBoards();
        log.info("BoardController.getAllBoards.out  Boards - {}", boards);
        return boards;
    }

    @Operation(summary = "Получить доску по id",
            description = "Возвращает доску с заданным id (Path Variable): с id, названием title, описанием description, " +
                    "временем последнего изменения timeOfLastChange и создания timeOfCreation, " +
                    "статусами Set<String> status и тегами Set<String> tags. Если в БД нет доски с таким id, " +
                    "бросает исключение 404")
    @GetMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    public Board getBoard(@PathVariable("id") long id) {
        log.info("BoardController.getBoard.in  id - {}", id);
        Board board = boardService.findBoard(id);
        if (board == null) {
            throw BOARD_NOT_FOUND.exception(String.valueOf(id));
        }
        log.info("BoardController.getBoard.out  Board - {}", board);
        return board;
    }

    @Operation(summary = "Удалить доску по id",
            description = "Удаляет доску с заданным id (Path Variable), а так же теги Set<String> tags, " +
                    "задачи Set<Task> tasks и статусы Set<String> status, связанные с ней")
    @DeleteMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    public void deleteBoard(@PathVariable("id") long id) {
        log.info("BoardController.deleteBoard.in id - {}", id);
        boardService.delete(id);
        log.info("BoardController.deleteBoard.out");
    }

    @Operation(summary = "Обновить доску по id",
            description = "Обновляет название title и описание description (not null) у доски (Request Body) " +
                    "с заданным id (Path Variable), обновляет время последнего изменения timeOfLastChange. " +
                    "Если в БД нет доски с таким id, бросает исключение 404")
    @PutMapping(path = "/{id}", produces = APPLICATION_JSON_VALUE)
    public void updateBoard(@RequestBody @Valid Board board, @PathVariable long id) {
        log.info("BoardController.updateBoard.in  Board - {}, id - {}.", board, id);
        if (boardService.findBoard(id) == null) {
            throw BOARD_NOT_FOUND.exception(String.valueOf(id));
        }
        boardService.update(board, id);
        log.info("BoardController.updateBoard.out");
    }

    @Operation(summary = "Добавить статус на доску по id",
            description = "Добавляет статус (Request Param) status (Например, TO_DO, IN_PROGRESS, и т.д.) на доску" +
                    " с заданным (Path Variable) id. Если в БД нет доски с таким id, бросает исключение 404")
    @PostMapping(path = "/{id}/status", produces = APPLICATION_JSON_VALUE)
    public void addStatus(@PathVariable("id") long id, @RequestParam String status) {
        log.info("BoardController.addStatus.in  id - {}, status - {}", id, status);
        if (boardService.findBoard(id) == null) {
            throw BOARD_NOT_FOUND.exception(String.valueOf(id));
        }
        boardService.addStatus(id, status);
        log.info("BoardController.addStatus.out");
    }

    @Operation(summary = "Удалить статус с доски по id",
            description = "Удаляет статус (Request Param) status с доски с заданным (Path Variable) id." +
                    " Если в БД нет доски с таким id, бросает исключение 404")
    @DeleteMapping(path = "/{id}/status", produces = APPLICATION_JSON_VALUE)
    public void deleteStatus(@PathVariable("id") long id, @RequestParam String status) {
        log.info("BoardController.addStatus.in  id - {}, status - {}", id, status);
        if (boardService.findBoard(id) == null) {
            throw BOARD_NOT_FOUND.exception(String.valueOf(id));
        }
        boardService.deleteStatus(id, status);
        log.info("BoardController.addStatus.out");
    }

    @Operation(summary = "Добавить задачу на доску с id",
            description = "Добавляет задачу (Request Body) task на доску с заданным (Path Variable) id с ненулевыми значениями: " +
                    "названием title, описанием description; может также сохранить статус status " +
                    "(если на доске нет этого статуса, будет присвоено значение null), имя работающего над ней сотрудника" +
                    "nameOfEmployee; присваивает id доски boardId, на которую задача добавляется, " +
                    "время создания timeOfCreation и время последнего изменения timeOfLastChange." +
                    " Если в БД нет доски с таким id, бросает исключение 404")
    @PostMapping(path = "/{id}/tasks", consumes = APPLICATION_JSON_VALUE)
    public void addTaskToBoard(@PathVariable long id, @RequestBody @Valid Task task) {
        log.info("TaskController.addTaskToBoard.in  id - {}, Task - {}", id, task);
        Board board = boardService.findBoard(id);
        if (board == null) {
            throw BOARD_NOT_FOUND.exception(String.valueOf(id));
        }
        taskService.addTaskToBoard(id, task);
        if (!board.getStatus().contains(task.getStatus())) {
            task.setStatus(null);
        }
        log.info("TaskController.addTaskToBoard.out title Task - {}", task);
        log.info("If status of added task does not match the specified in request, " +
                "please check available status on the board and update task.");
    }

    @Operation(summary = "Получить все задачи с доски по id",
            description = "Возвращает все задачи с доски с заданным id (Path Variable): с id, названием title, " +
                    "описанием description, id доски boardId, временем последнего изменения timeOfLastChange и " +
                    "создания timeOfCreation, именем сотрудника nameOfEmployee, статусом status и тегами " +
                    "Set<String> tags. Если в БД нет доски с таким id, бросает исключение 404")
    @GetMapping(path = "/{id}/tasks", produces = APPLICATION_JSON_VALUE)
    public List<Task> getAllTasksFromBoard(@PathVariable long id) {
        log.info("TaskController.getAllTasksFromBoard.in  id - {}", id);
        Board board = boardService.findBoard(id);
        if (board == null) {
            throw BOARD_NOT_FOUND.exception(String.valueOf(id));
        }
        List<Task> tasks = taskService.allTasksFromBoard(id);
        log.info("TaskController.getAllTasksFromBoard.out  Tasks - {}", tasks);
        return tasks;
    }

}
