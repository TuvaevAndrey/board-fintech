package com.fintech.controller;

import com.fintech.model.Board;
import com.fintech.model.Note;
import com.fintech.model.Schedule;
import com.fintech.model.Task;
import com.fintech.service.*;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

import static com.fintech.exception.AppExceptions.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@OpenAPIDefinition(
        info = @Info(
                title = "tag-controller",
                version = "1.0",
                description = "Контроллер для взаимодействия с тегами"
        )
)
@RestController
@RequestMapping("/tags")
@RequiredArgsConstructor
@Slf4j
public class TagController {

    private final TagService tagService;
    private final NoteService noteService;
    private final BoardService boardService;
    private final ScheduleService scheduleService;
    private final TaskService taskService;

    @Operation(summary = "Добавить тег на заметку с id",
            description = "Добавляет тег (Request Param) tag на заметку с заданным (Path Variable) id. " +
                    "Если заметки с этим id не существует, будет выброшено исключение 404. " +
                    "В базу теги записываются строчными буквами.")
    @PostMapping(path = "/notes/{noteId}", produces = APPLICATION_JSON_VALUE)
    public void addTagToNote(@PathVariable long noteId, @RequestParam String tag) {
        log.info("TagController.addTagToNote.in id - {}, tag - {}.", noteId, tag);
        Note note = noteService.findNote(noteId);
        if (note == null) {
            throw NOTE_NOT_FOUND.exception(String.valueOf(noteId));
        }
        tagService.addTagToNote(noteId, tag);
        log.info("TagController.addTagToNote.out");
    }

    @Operation(summary = "Добавить тег на доску с id",
            description = "Добавляет тег (Request Param) tag на доску с заданным (Path Variable) id. " +
                    "Если доски с этим id не существует, будет выброшено исключение 404. " +
                    "В базу теги записываются строчными буквами.")
    @PostMapping(path = "/boards/{boardId}", produces = APPLICATION_JSON_VALUE)
    public void addTagToBoard(@PathVariable long boardId, @RequestParam String tag) {
        log.info("TagController.addTagToBoard.in  id - {}, tag - {}.", boardId, tag);
        Board board = boardService.findBoard(boardId);
        if (board == null) {
            throw BOARD_NOT_FOUND.exception(String.valueOf(boardId));
        }
        tagService.addTagToBoard(boardId, tag);
        log.info("TagController.addTagToBoard.out");
    }

    @Operation(summary = "Добавить тег на планировщик с id",
            description = "Добавляет тег (Request Param) tag на планировщик с заданным (Path Variable) id. " +
                    "Если планировщика с этим id не существует, будет выброшено исключение 404. " +
                    "В базу теги записываются строчными буквами.")
    @PostMapping(path = "/schedules/{scheduleId}", produces = APPLICATION_JSON_VALUE)
    public void addTagToSchedule(@PathVariable long scheduleId, @RequestParam String tag) {
        log.info("TagController.addTagToSchedule.in id - {}, tag - {}.", scheduleId, tag);
        Schedule schedule = scheduleService.findScheduleById(scheduleId);
        if (schedule == null) {
            throw SCHEDULE_NOT_FOUND.exception(String.valueOf(scheduleId));
        }
        tagService.addTagToSchedule(scheduleId, tag);
        log.info("TagController.addTagToSchedule.out");
    }

    @Operation(summary = "Добавить тег на задачу с id",
            description = "Добавляет тег (Request Param) tag на задачу с заданным (Path Variable) id. " +
                    "Если задачи с этим id не существует, будет выброшено исключение 404. " +
                    "В базу теги записываются строчными буквами.")
    @PostMapping(path = "/boards/tasks/{taskId}", produces = APPLICATION_JSON_VALUE)
    public void addTagToTask(@PathVariable long taskId, @RequestParam String tag) {
        log.info("TagController.addTagToTask.in  taskId - {}, tag - {}.", taskId, tag);
        Task task = taskService.findTaskById(taskId);
        if (task == null) {
            throw TASK_NOT_FOUND.exception(String.valueOf(taskId));
        }
        tagService.addTagToTask(taskId, tag);
        log.info("TagController.addTagToTask.out");
    }

    @Operation(summary = "Найти все объекты с заданным тегом",
            description = "Возвращает список объектов (заметок, досок, задач и планировщиков) с указанным " +
                    "тегом tag (Request Param). Если объектов с этим тегом не существует, " +
                    "будет выброшено исключение 404. Регистр тега не имеет значения.")
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public List<Object> findByTag(@RequestParam String tag) {
        log.info("TagController.findByTag.in  tag - {}.", tag);
        if (!tagService.allTags().contains(tag.toLowerCase())) {
            throw TAG_NOT_FOUND.exception(tag);
        }
        List<Object> entities = tagService.findSomethingByTag(tag);
        log.info("TagController.findByTag.out  entities - {}.", entities);
        return entities;
    }

    @Operation(summary = "Найти все заметки с заданным тегом",
            description = "Возвращает список заметок с указанным тегом tag (Request Param). Если заметок с " +
                    "этим тегом не существует, будет выброшено исключение 404. Регистр тега не имеет значения.")
    @GetMapping(path = "/notes", produces = APPLICATION_JSON_VALUE)
    public List<Note> findNotesByTag(@RequestParam String tag) {
        log.info("TagController.findNotesByTag.in  tag - {}.", tag);
        List<Note> notes = tagService.findNotesByTag(tag);
        if (notes.isEmpty()) {
            throw TAG_ON_NOTE_NOT_FOUND.exception(tag);
        }
        log.info("TagController.findNotesByTag.out  notes - {}.", notes);
        return notes;
    }

    @Operation(summary = "Получить все теги с заметки по id",
            description = "Возвращает список тегов с заметки с заданным id (PathVariable). Если в БД нет заметки " +
                    "с таким id, будет выброшено исключение 404.")
    @GetMapping(path = "/notes/{noteId}", produces = APPLICATION_JSON_VALUE)
    public Set<String> getTagsFromNote(@PathVariable long noteId) {
        log.info("TagController.getTagsFromNote.in  id - {}.", noteId);
        Note note = noteService.findNote(noteId);
        if (note == null) {
            throw NOTE_NOT_FOUND.exception(String.valueOf(noteId));
        }
        Set<String> tags = note.getTags();
        log.info("TagController.getTagsFromNote.out  tags - {}.", tags);
        return tags;
    }

    @Operation(summary = "Найти все доски с заданным тегом",
            description = "Возвращает список досок с указанным тегом tag (Request Param). Если досок с этим тегом " +
                    "не существует, будет выброшено исключение 404. Регистр тега не имеет значения.")
    @GetMapping(path = "/boards", produces = APPLICATION_JSON_VALUE)
    public List<Board> findBoardsByTag(@RequestParam String tag) {
        log.info("TagController.findBoardsByTag.in  tag - {}.", tag);
        List<Board> boards = tagService.findBoardsByTag(tag);
        if (boards.isEmpty()) {
            throw TAG_ON_BOARD_NOT_FOUND.exception(tag);
        }
        log.info("TagController.findBoardsByTag.out  boards - {}.", boards);
        return boards;
    }

    @Operation(summary = "Получить все теги с доски по id",
            description = "Возвращает список тегов с доски с заданным id (PathVariable). Если в БД нет доски " +
                    "с таким id, будет выброшено исключение 404.")
    @GetMapping(path = "/boards/{boardId}", produces = APPLICATION_JSON_VALUE)
    public Set<String> getTagsFromBoard(@PathVariable long boardId) {
        log.info("TagController.getTagsFromBoard.in  id - {}.", boardId);
        Board board = boardService.findBoard(boardId);
        if (board == null) {
            throw BOARD_NOT_FOUND.exception(String.valueOf(boardId));
        }
        Set<String> tags = board.getTags();
        log.info("TagController.getTagsFromBoard.out  tags - {}.", tags);
        return tags;
    }

    @Operation(summary = "Найти все задачи с заданным тегом",
            description = "Возвращает список задач с указанным тегом tag (Request Param). Если задач с этим тегом " +
                    "не существует, будет выброшено исключение 404. Регистр тега не имеет значения.")
    @GetMapping(path = "/boards/tasks", produces = APPLICATION_JSON_VALUE)
    public List<Task> findTasksByTag(@RequestParam String tag) {
        log.info("TagController.findTasksByTag.in  tag - {}.", tag);
        List<Task> tasks = tagService.findTasksByTag(tag);
        if (tasks.isEmpty()) {
            throw TAG_ON_TASK_NOT_FOUND.exception(tag);
        }
        log.info("TagController.findTasksByTag.out  tasks - {}.", tasks);
        return tasks;
    }

    @Operation(summary = "Получить все теги с задачи по id",
            description = "Возвращает список тегов с задачи с заданным id (PathVariable). Если в БД нет задачи " +
                    "с таким id, будет выброшено исключение 404.")
    @GetMapping(path = "/boards/tasks/{taskId}", produces = APPLICATION_JSON_VALUE)
    public Set<String> getTagsFromTask(@PathVariable long taskId) {
        log.info("TagController.getTagsFromTask.in  id - {}.", taskId);
        Task task = taskService.findTaskById(taskId);
        if (task == null) {
            throw TASK_NOT_FOUND.exception(String.valueOf(taskId));
        }
        Set<String> tags = task.getTags();
        log.info("TagController.getTagsFromTask.out  tags - {}.", tags);
        return tags;
    }

    @Operation(summary = "Найти все планировщики с заданным тегом",
            description = "Возвращает список планировщиков с указанным тегом tag (Request Param). Если планировщиков " +
                    "с этим тегом не существует, будет выброшено исключение 404. Регистр тега не имеет значения.")
    @GetMapping(path = "/schedules", produces = APPLICATION_JSON_VALUE)
    public List<Schedule> findSchedulesByTag(@RequestParam String tag) {
        log.info("TagController.findSchedulesByTag.in  tag - {}.", tag);
        List<Schedule> schedules = tagService.findSchedulesByTag(tag);
        if (schedules.isEmpty()) {
            throw TAG_ON_SCHEDULE_NOT_FOUND.exception(tag);
        }
        log.info("TagController.findSchedulesByTag.out  schedules - {}.", schedules);
        return schedules;
    }

    @Operation(summary = "Получить все теги с планировщика по id",
            description = "Возвращает список тегов с планировщика с заданным id (PathVariable). Если в БД нет " +
                    "планировщика с таким id, будет выброшено исключение 404.")
    @GetMapping(path = "/schedules/{scheduleId}", produces = APPLICATION_JSON_VALUE)
    public Set<String> getTagsFromSchedule(@PathVariable long scheduleId) {
        log.info("TagController.getTagsFromSchedule.in  id - {}.", scheduleId);
        Schedule schedule = scheduleService.findScheduleById(scheduleId);
        if (schedule == null) {
            throw SCHEDULE_NOT_FOUND.exception(String.valueOf(scheduleId));
        }
        Set<String> tags = schedule.getTags();
        log.info("TagController.getTagsFromSchedule.out  tags - {}.", tags);
        return tags;
    }

    @Operation(summary = "Удалить тег с заметки по id",
            description = "Удаляет тег tag (RequestParam) с заметки с заданным id (PathVariable).")
    @DeleteMapping(path = "/notes/{noteId}", produces = APPLICATION_JSON_VALUE)
    public void deleteTagFromNote(@PathVariable long noteId, @RequestParam String tag) {
        log.info("TagController.deleteTagFromNote.in id - {}, tag - {}.", noteId, tag);
        tagService.deleteTagFromNote(noteId, tag);
        log.info("TagController.deleteTagFromNote.out");
    }

    @Operation(summary = "Удалить тег с доски по id",
            description = "Удаляет тег tag (RequestParam) с доски с заданным id (PathVariable).")
    @DeleteMapping(path = "/boards/{boardId}", produces = APPLICATION_JSON_VALUE)
    public void deleteTagFromBoard(@PathVariable long boardId, @RequestParam String tag) {
        log.info("TagController.deleteTagFromBoard.in id - {}, tag - {}.", boardId, tag);
        tagService.deleteTagFromBoard(boardId, tag);
        log.info("TagController.deleteTagFromBoard.out");
    }


    @Operation(summary = "Удалить тег с задачи по id",
            description = "Удаляет тег tag (RequestParam) с задачи с заданным id (PathVariable).")
    @DeleteMapping(path = "/boards/tasks/{taskId}", produces = APPLICATION_JSON_VALUE)
    public void deleteTagFromTask(@PathVariable long taskId, @RequestParam String tag) {
        log.info("TagController.deleteTagFromTask.in  taskId - {}, tag - {}.", taskId, tag);
        tagService.deleteTagFromTask(taskId, tag);
        log.info("TagController.deleteTagFromTask.out");
    }


    @Operation(summary = "Удалить тег с планировщика по id",
            description = "Удаляет тег tag (RequestParam) с планировщика с заданным id (PathVariable).")
    @DeleteMapping(path = "/schedules/{scheduleId}", produces = APPLICATION_JSON_VALUE)
    public void deleteTagFromSchedule(@PathVariable long scheduleId, @RequestParam String tag) {
        log.info("TagController.deleteTagFromSchedule.in id - {}, tag - {}.", scheduleId, tag);
        tagService.deleteTagFromSchedule(scheduleId, tag);
        log.info("TagController.deleteTagFromSchedule.out");
    }

}
